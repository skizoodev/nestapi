import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import {ValidationPipe} from "@nestjs/common"
import {HttpExceptionFilter} from "./middleware/exception.middleware"
import * as bodyParser from "body-parser"
async function start() {
  const app = await NestFactory.create(MainModule);
  app.useGlobalPipes(new ValidationPipe())
  app.useGlobalFilters(new HttpExceptionFilter())
  await app.listen(3000);
}
start();
