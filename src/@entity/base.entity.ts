import { CreateDateColumn,PrimaryGeneratedColumn,Column } from 'typeorm';
export abstract class Bmain {
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:"varchar",length:45})
    name:string
    @Column({type:"varchar",length:45})
    img:string
    @CreateDateColumn({name:"created_at"})
    created_at:Date
}
export abstract class Bchat { 
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:"varchar",length:45})
    sender:string
    @Column({type:"varchar",length:45})
    reciver:string
    @Column({type:"varchar",length:225})
    content:string
    @CreateDateColumn({name:"created_at"})
    createdAt:Date
}
export abstract class Badittional{
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:'varchar',length:100})
    name:string
    @Column({type:'varchar',length:100})
    value:string
}

