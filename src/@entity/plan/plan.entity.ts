import {Column,Entity,PrimaryGeneratedColumn,OneToOne,JoinColumn,CreateDateColumn
    } from "typeorm"
    import { addMonths } from 'date-fns';
 import {Bmain} from "../base.entity"
 import {User} from "../clients/profile.entity"
 @Entity("Plan")
export class Plan extends Bmain{
       // one to one relation with user and prevelige
       @OneToOne(() => Benefit, (ben) => ben.id,{cascade:true})
       @JoinColumn({name:"previlage"})
       readonly benefit:number
}
@Entity("Benefit")
export class Benefit {
    @PrimaryGeneratedColumn()
    id:number
    @Column({type:"int"})
    senderF:number
    @Column({type:"decimal"})
    depositF:number
    @Column({type:"int"})
    discount:number
    @Column({type:"int"})
    retreat:number
}
@Entity("Subscribtion")
export class subscribtion{
    @PrimaryGeneratedColumn()
    id:number
    @CreateDateColumn({name:"SubDate"})
    SubDate:Date
    @Column({type:"date"})
    expiresAt:Date 
    @OneToOne(() => Plan, (plan) => plan.id,{cascade:true})
    @JoinColumn({name:"PalnId"})
    readonly PlanId:number
}