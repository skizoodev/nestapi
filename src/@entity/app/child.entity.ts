import { Column,Entity,ManyToOne,
    PrimaryGeneratedColumn } from "typeorm"
import { User } from "../clients/profile.entity"
@Entity("Children")
export class Child{
    @PrimaryGeneratedColumn()
    id:number
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly username:string
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly password:string
    @Column({type:'varchar',length:25,nullable:false})
    readonly fname:string
    @Column({type:'varchar',length:25,nullable:false})
    readonly lname:string
    @Column({type:'varchar',length:25,nullable:false})
    readonly avatar:string
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly company:string 
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly role:string
         //relation many to one Email with user
         @ManyToOne(() => User, { cascade: true })
         father: number;
  }
