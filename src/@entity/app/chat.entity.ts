import { Column, Entity,ManyToOne
    ,CreateDateColumn,PrimaryGeneratedColumn 
} from "typeorm"
import {User} from "../clients/profile.entity"
@Entity("Email")
export class Email  {
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:"varchar",length:100})
    title:string
    @Column({type:"varchar",length:225})
    content:string
     //relation many to one Email with user
     @ManyToOne(() => User, { eager: true })
     sender: User;
    @ManyToOne(() => User, { eager: true })
     recipient: User;
     @CreateDateColumn({name:"created_at"})
     createdAt:Date
    }
@Entity("Chat")
export class Chat {
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:"varchar",length:100})
    message:string
    //relation many to one Email with user
    @ManyToOne(() => User, { eager: true })
    sender: User;
   @ManyToOne(() => User, { eager: true })
    recipient: User;
    @CreateDateColumn({name:"created_at"})
    createdAt:Date
}