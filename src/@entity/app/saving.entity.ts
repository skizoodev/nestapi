import { Column, Entity,ManyToOne,JoinTable,ManyToMany} from "typeorm"
import {User} from "../clients/profile.entity"
import {Bmain} from "../base.entity"
@Entity("SavCat")
export class SavingCat extends Bmain{}
@Entity("Saving")
export class SavingProduct extends Bmain {
    @Column()
    description:string
    @Column({type:"int",default:0})
    minbudget:number
    @Column({type:"int",default:0})
    amount:number
    @ManyToMany(() => SavingCat)
    @JoinTable()
    categories: SavingCat[]
    @ManyToOne(() => User, (user) => user.saving)
    joinner: User
    //relation many to one  with user
/*     @ManyToOne(() => User, (user) => user.saving)
    user: number */
}