import { Column, Entity,ManyToOne,PrimaryGeneratedColumn 
} from "typeorm"
import {User} from "../clients/profile.entity"
@Entity("Calander")
export class Calander  {
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:"varchar",length:25})
    title:string
    @Column({type:"varchar",length:25})
    filter:string
    @Column({type:"varchar",length:225})
    detail:string
    @Column({type:"date"})
    dated:Date
    @Column({type:"date"})
    datef:Date
     //relation many to one calnder with user
     @ManyToOne(() => User, { eager: true,cascade:true })
     userId: number;
    }