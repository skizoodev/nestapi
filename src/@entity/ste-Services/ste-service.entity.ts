import { Column, Entity,ManyToMany,JoinTable, PrimaryGeneratedColumn,OneToMany,ManyToOne } from "typeorm"
import { Bmain } from "../base.entity"
@Entity("CatService")
export class FaCategorie extends Bmain{
 //relation many to one Categorie with facture
 @ManyToMany(() => SteFacture,{cascade:true})
 @JoinTable({name:"PCM"})
 factures: number[]
}
@Entity("steProduct")
export class SteFacture extends Bmain {
    @Column({type:"varchar",length:100})
    desc:string
    @OneToMany(() => Attribute,(extra) => extra.facture)
   readonly attributes: number[]
}
@Entity("productAtt")
export class Attribute  {
@PrimaryGeneratedColumn({type:"int"})
id:number
@Column({type:"varchar"})
name:string
@Column({type:"varchar"})
value:string
@ManyToOne(() => SteFacture, (facture) => facture.id,{cascade:true})
facture: number
}