import { Column, Entity, PrimaryGeneratedColumn,ManyToOne,CreateDateColumn } from "typeorm"
import { User}from "../clients/profile.entity"
@Entity("Amount")
export class Amount {
    @PrimaryGeneratedColumn({type:'int'})
    id:number
    @Column({type:"float",default:0.0})
    solde:number
    @Column({type:"float",default:0})
    pt:number
    @Column({type:"float",default:0})
    cashback:number
    @Column({type:"float",default:0})
    remise:number
}
@Entity("Invoices")
export class Invoice{
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:'varchar',length:20})
    faAmount:string
    @Column({type:'varchar',length:20})
    type:string
    @Column({type:'varchar',length:20})
    status:string
    @Column({type:'varchar',length:200})
    faDetail:string
    @Column({type:'varchar',length:200})
    url:string
    @CreateDateColumn({name:"created_at"})
    createdAt:Date
    //realation many to one amount with user
    @ManyToOne(() => User, (user) => user.invoice,{cascade:true})
    userId: number
}
@Entity("recu")
export class Recu{
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @Column({type:'varchar',length:20})
    faAmount:string
    @Column({type:'varchar',length:20})
    type:string
    @Column({type:'varchar',length:20})
    faFrais:string
    @Column({type:'varchar',length:100})
    faDetail:string
    @CreateDateColumn({name:"created_at"})
    createdAt:Date
    //realation many to one amount with user
    @ManyToOne(() => User, (user) => user.recu,{cascade:true})
    userId: number
}