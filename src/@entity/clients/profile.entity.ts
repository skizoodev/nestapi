import { Column,Entity,OneToOne, 
    OneToMany, JoinColumn,ManyToOne,
    PrimaryGeneratedColumn ,Index,JoinTable,ManyToMany} from "typeorm"
import {Additional,Tenant} from "./addintional.entity"
import {Amount,Invoice,Recu} from "../currency/amount.entity"
import {Chat,Email} from "../app/chat.entity"
import {SavingProduct} from "../app/saving.entity"
import {subscribtion} from "../plan/plan.entity"
import { Calander } from "../app/calander.entity"
import { Child } from "../app/child.entity"
export enum Sexe {
    Male = "Homme",
    Famle = "Femme",
    Gmail = "Autre",
    Ste="Sociètè"
}
//entity for User Profile 
@Entity("Profile")
@Index(["cin","fname","lname"])
export class User{
    @PrimaryGeneratedColumn()
    id:number
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly cin:string
    @Column({type:'varchar',length:25,nullable:false})
    readonly fname:string
    @Column({type:'varchar',length:25,nullable:false})
    readonly lname:string
    @Column({type:"enum", enum:Sexe,default: Sexe.Male})
    readonly sexe:Sexe
    @Column({type:'varchar',length:25,nullable:false})
    readonly avatar:string
    @Column({type:'varchar',length:25,nullable:false,unique:true})
    readonly email:string
     @Column({type:'varchar',length:30,nullable:false,unique:true})
    readonly room:string
    @Column({type:"datetime"})
    readonly birthday:Date
          // one to one relation with user and currency
          @OneToOne(() => Amount, (currency) => currency.id,{cascade:true})
          @JoinColumn({name:"amountId"})
          readonly currency:number
        //define assosiation between client and Adresse 
        @OneToMany(()=>Recu,(recu)=>recu.id,{cascade:true})
        @JoinColumn({name:"recu"})
        readonly recu:number[]
        //define assosiation between client and Adresse 
        @OneToMany(()=>Calander,(recu)=>recu.id,{cascade:true})
        @JoinColumn({name:"events"})
        readonly events:number[]
           // one to one relation with user and Plan
           @OneToOne(() => subscribtion, (plan) => plan.id,{cascade:true})
           @JoinColumn({name:"subscribtion"})
           readonly subscribtion:number
        //define assosiation between client and Adresse 
        @OneToMany(()=>Tenant,(soukna)=>soukna.id,{cascade:true})
        @JoinColumn({name:"Tenant"})
        readonly tenant_Id:number[]
      //define assosiation between client and mail  
        @OneToMany(()=>Email,(mail)=>mail.sender,{cascade:true})
        readonly sentMails:number[]
        @OneToMany(()=>Email,(mail)=>mail.recipient,{cascade:true})
        readonly recivedMails:number[]
         //define assosiation between client and chat   
         @OneToMany(()=>Chat,(chat)=>chat.sender,{cascade:true})
        readonly sentChat:number[]
        @OneToMany(()=>Chat,(chat)=>chat.recipient,{cascade:true})
        readonly recivedMesseges:number[]
        //define assosiation between user  and user  
          @ManyToMany(() => Favories,{cascade:true})
         @JoinTable()
         readonly  favories: number[];
         @ManyToMany(() =>Report,{cascade:true})
         @JoinTable()
         readonly  reported: number[];
         @OneToMany(()=>Child,(chat)=>chat.father)
         readonly children:number[]
          // many to one relation with user and Saving
         @ManyToOne(() => SavingProduct, (loan) => loan.joinner,{cascade:true})
         @JoinColumn({name:"savingId"})
         readonly saving: number[]
    //up is Done an tested succ🖕🏽🖕🏽🖕🏽🖕🏽
   
    //define assosiation between client and Extra-Information 
    @OneToMany(() => Additional, (extra) => extra.user)
    readonly extra: number[]
    // many to one relation with user and Invoices
    @OneToMany(() => Invoice, (invoices) => invoices.userId)
    readonly invoice: Invoice[]
}
@Entity("Report")
export class Report{
  @PrimaryGeneratedColumn()
  id:number
  @Column({type:'varchar',length:225})
  reason:string
  @Column({type:'varchar',length:25,nullable:false})
  categories:string

}
@Entity("Favories")
export class Favories{
  @PrimaryGeneratedColumn()
  id:number
}