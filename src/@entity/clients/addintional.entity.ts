import {Column,Entity,PrimaryGeneratedColumn,
   ManyToOne,ManyToMany,JoinTable} from "typeorm"
import  {User} from "./profile.entity"
import {Bmain,Badittional} from "../base.entity"
@Entity("Adress")
export class Tenant extends Bmain{
    @Column({type:'varchar',length:45})
    address:string
    @Column({type:'varchar',length:45})
    zipcode:string
}
@Entity("Extra-Information")
export class Additional extends Badittional{
    //assocciation between user and extra data like fb insta ...
    @ManyToOne(() => User, (user) => user.extra)
    user: User
}


@Entity("Friend")
export class Friend{
    @PrimaryGeneratedColumn({type:"int"})
    id:number
    @ManyToMany(() => User)
    @JoinTable({name:"Fellowers"})
    friend: User[]
}