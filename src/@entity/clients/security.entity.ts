import { Entity,Column,CreateDateColumn,
    PrimaryGeneratedColumn,OneToOne,JoinColumn,
     ManyToOne, OneToMany, Unique} from 'typeorm';
import { User } from './profile.entity';
//define array of state of user
export enum AuthState {
    GUEST = "guest",
    BAN = "banned",
    USER="user",
    VERIFIED = "verified"
}
//entity for Login 
@Entity("Auth")
export class Auth{
@PrimaryGeneratedColumn({type:"int"})
id:number
@Column({type:"varchar",length:25,unique:true,nullable:true})
username:string
@Column({type:"varchar",length:125})
password:string
@Column({type:"varchar",length:125})
secret:string
@Column({type:"varchar",default:"user"})
role:string
@Column({type:'boolean',default:false})
security:boolean
//define assosiation between auth  and email sec 
@OneToMany(() => EmailSec, (sec) => sec.authId)
readonly esId: number[]
@OneToMany(() => Phone, (sec) => sec.authId)
readonly psId: number[]
//define assosiation between Security and auth 
@OneToOne(()=>Question,(auth)=>auth.id,{cascade:true})
@JoinColumn({name:"questionId"})
readonly qsId: number
  //define assosiation between Profile and auth***
/*   @OneToOne(()=>User,(cliend)=>cliend.id,{cascade:true})
  @JoinColumn({name:"cliendId"})
  readonly cliendId:number */
      //define assosiation between client and Plan 
/*       @OneToOne(()=>Plan,(plan)=>plan.id,{cascade:true})
      @JoinColumn({name:"planId"})
      readonly plan:number */
// up are done 
//define assosiation between Profile and auth 
@OneToMany(()=>LoginLog,(args)=>args.user)
logins:LoginLog[]
}
//entity for question security 
@Entity("QtSecure")
export class Question{
@PrimaryGeneratedColumn({type:"int"})
id:number
@Column({type:"varchar"})
question:string
@Column({type:"varchar"})
response:string
}
//entity for email security 
@Entity("EmSecure")
export class EmailSec{
@PrimaryGeneratedColumn({type:"int"})
id:number
@Column({type:"varchar",length:"50",unique:true})
email:string
@Column({type:"boolean",default:false})
isverfied:boolean
//define assosiation between Security and auth 
@ManyToOne(()=>Auth,(auth)=>auth.id,{cascade:true})
readonly authId: number
}
//entity for phone security 
@Entity("PhSecure")
export class Phone{
@PrimaryGeneratedColumn({type:"int"})
id:number
@Column({type:"varchar",length:"20"})
phone:string
@Column({type:"boolean",default:false})
isverfied:boolean
//define assosiation between NumberSecurity and auth 
@ManyToOne(()=>Auth,(auth)=>auth.id,{cascade:true})
readonly authId: number
}
//entity for login log security 
@Entity("LoginLog")
export class LoginLog {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn({name:"logintime"})
  created_at:Date
  @Column()
  browser: string;
  @Column()
  engine: string;
  @Column()
  device: string;
    @ManyToOne(()=>Auth,(args)=>args.logins)
    user:Auth
}