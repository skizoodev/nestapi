import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import {ClientAuthService} from "../modules/auth/services/auth.service"
@Injectable()
export class PasswordGuard implements CanActivate {
    constructor(
        private readonly ppt:ClientAuthService
    ){}
  async canActivate(
    context: ExecutionContext,
    ): Promise<boolean> {
      const request = context.switchToHttp().getRequest();
       const userId=request.userId
      const secret= request.body.secret;
       const user = await this.ppt.ValidatePassword(userId,secret);
       if(user=="wrong password"){
        return false
       }
       
  return true
    }
  
}
