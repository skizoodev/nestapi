import { Injectable ,ExecutionContext ,CanActivate,ForbiddenException} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from '../decorator/roles.decorator';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {}
@Injectable()
export class JwtAuthGuard  extends AuthGuard('jwt') implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly reflector: Reflector,
  ) {super()}

  canActivate(context: ExecutionContext):boolean|Promise<boolean>|Observable<boolean> {
    const requiredRoles = this.reflector.getAllAndMerge<string[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers.authorization;
    const token = authHeader?.split(' ')[1];
    if (!token) {
      return false;
    }
    try {
      const decoded =  this.jwtService.verify(token);
      const userRoles="user"
      request.userId=decoded.userId
      //const userRoles = decoded.role;
      return requiredRoles.some((role) => userRoles?.includes(role));
    } catch (err) {
      console.log(err)
      throw new ForbiddenException( 403, { cause: new Error(err) })
      //return false;
    }
  
}

}
