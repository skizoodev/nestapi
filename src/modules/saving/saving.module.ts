import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {AdminSavingCon,ClientSavingController} from "./controller/saving.controller"
import { AdminSavingService,ClientSavingService} from "./service/saving.service"
import {Saving} from "../../@entity/app/saving.entity"
@Module({
    controllers:[ClientSavingController,AdminSavingCon],
    imports:[TypeOrmModule.forFeature([Saving])],
    providers:[AdminSavingService,ClientSavingService]
})
export class SavingModule {}
