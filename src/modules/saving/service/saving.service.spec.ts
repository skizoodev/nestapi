import { Test, TestingModule } from '@nestjs/testing';
import { AdminSavingService,ClientSavingService } from './saving.service';

describe('SavingService', () => {
  let service: AdminSavingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdminSavingService],
    }).compile();

    service = module.get<AdminSavingService>(AdminSavingService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
