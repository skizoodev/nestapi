import { Injectable } from '@nestjs/common';
import { Saving } from "../../../@entity/app/saving.entity"
import { Amount, Invoice } from "../../../@entity/currency/amount.entity"
import { User } from "../../../@entity/clients/profile.entity"
import { InjectRepository, InjectEntityManager } from "@nestjs/typeorm"
import { Repository, EntityManager } from "typeorm"
import { Cloan, Uloan } from "../saving.dto"
@Injectable()
export class AdminSavingService {
    constructor(
        @InjectRepository(Saving)
        private readonly Loan: Repository<Saving>
    ) {}
    async create(arg: Cloan) {
       return await this.Loan.createQueryBuilder().insert().values(arg).execute()
    }
    async update(arg: Uloan) {
        await this.Loan.createQueryBuilder().update().set(arg).where({ name: arg.name }).execute()
    }
    async Delete(arg: Uloan) {
        await this.Loan.createQueryBuilder().delete().where({ name: arg.name }).execute()
    }
}
export class ClientSavingService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager: EntityManager,
    ) { }

    async findAll() {
            return await this.entityManager.getRepository(Saving).createQueryBuilder().getMany()
    }
    async Findcustom(arg: Uloan) {
        return await this.entityManager.getRepository(Saving).createQueryBuilder().where({ name: arg.name }).getOne()
    }
    async subscribe(userId: 1, saving: 1) {
        return await  this.entityManager.getRepository(User).createQueryBuilder().relation(User, "saving").of(userId).set(saving)
    }
    async deposit(amount: 100, saving: 1) {
        return this.entityManager.transaction(async (t) => {
            try {
                await t.decrement(Amount, { id: 1 }, "solde", amount);
                await t.increment(Saving, { id: saving }, "current", amount)
                await t.getRepository(Invoice).createQueryBuilder().insert().values({
                    faAmount: amount.toString(), status: "sucess", url: "current url", faDetail: "deposit amount to current saving"
                }).execute()
                return "done"
            } catch (error) {
                return await t.getRepository(Invoice).createQueryBuilder().insert().values({
                    faAmount: amount.toString(), status: "failed", url: "current url", faDetail: "deposit amount to current saving"
                }).execute()
            }
        })
    }
    async getSaving(){
        return await this.entityManager.getRepository(User)
        .createQueryBuilder().relation(User,"saving").of(1).loadOne()
    }
}