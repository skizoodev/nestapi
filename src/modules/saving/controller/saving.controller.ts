import { Controller,Post,Body,Put,Delete,
    Get,HttpCode,HttpStatus,HttpException 
} from '@nestjs/common';
import {AdminSavingService,ClientSavingService} from "../service/saving.service"
import {Cloan,Uloan} from "../saving.dto"
@Controller('Admiro/v1/saving')
export class AdminSavingCon {
constructor(private readonly savCon:AdminSavingService){}
@Post("create")
@HttpCode(201)

async Create(@Body() arg:Cloan){
    try {
        await this.savCon.create(arg)
        return  HttpStatus.ACCEPTED
    } catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@Put("edit")
@HttpCode(201)
async Edite(@Body()arg:Uloan){
    try{
    await this.savCon.update(arg)
    return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Delete("remove")
@HttpCode(201)
async ByeBye(@Body()arg:Uloan){
    try{
    await this.savCon.Delete(arg)
    return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}
@Controller('api/v1/saving')
export class ClientSavingController {
constructor(private readonly savCon:ClientSavingService){}
@Get("findall")
@HttpCode(202)
async getAll(){
    try{
    const product=await this.savCon.findAll()
    return  HttpStatus.ACCEPTED,product
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}

}
@Get("one")
@HttpCode(202)
async Edite(){
    try{
    const loan=await this.savCon.getSaving()
    return  HttpStatus.ACCEPTED,loan
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Post("subscribe")
@HttpCode(202)
async subscribe(userId: 1, saving: 1){
    try{
    await this.savCon.subscribe(userId,saving)
    return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}