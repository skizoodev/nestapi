import {IsNotEmpty,IsString,IsNumber } from 'class-validator';
import {PartialType} from "@nestjs/mapped-types"
export class Cloan {
    @IsNotEmpty()
    @IsString()
    name:string
    @IsNotEmpty()
    @IsString()
    img:string
}
export class Uloan extends PartialType(Cloan){}