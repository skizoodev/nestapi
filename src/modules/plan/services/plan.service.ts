import { Injectable,} from "@nestjs/common"
import {Repository} from "typeorm"
import {InjectRepository} from "@nestjs/typeorm"
import {Plan} from "../../../@entity/plan/plan.entity"
import {CreatePlanDto} from "../plan.dto"
@Injectable()
export class AdminPlanServ{
    constructor(
        @InjectRepository(Plan)
        private planRepository: Repository<Plan>
      ) {}
async Create (data:CreatePlanDto){
        return await this.planRepository.createQueryBuilder().insert()
        .values(data).execute()
} 
async Update (data:CreatePlanDto){
    return await this.planRepository.createQueryBuilder().update().set(data)
    .where({name:data.name}).execute() 
}
async Delete (id:number){
   return await this.planRepository.createQueryBuilder().delete().
   where({id:id}).execute()
}
}
