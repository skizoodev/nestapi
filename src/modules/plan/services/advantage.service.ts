import { Injectable,} from "@nestjs/common"
import {EntityManager} from "typeorm"
import { addMonths } from 'date-fns';
import {InjectEntityManager} from "@nestjs/typeorm"
import {Benefit,Plan,subscribtion} from "../../../@entity/plan/plan.entity"
import {CreateBenefitDto,CreateSubDto} from "../plan.dto"
import { User } from "src/@entity/clients/profile.entity";
@Injectable()
export class AdminAdvantageSrv{
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
      ) {}
async create(data:CreateBenefitDto){
    return this.entityManager.transaction(async(t)=>{
        const create= await t.getRepository(Benefit).createQueryBuilder().
        insert().into(Benefit).values({
            depositF:data.depositF,discount:data.discount,retreat:data.retreat,senderF:data.senderF
        }).execute()
        await t.getRepository(Plan).createQueryBuilder()
        .relation(Plan,"benefit").of(create.raw.insertId).set(data.plantId) 
    })
}
async Update (data:CreateBenefitDto,id:number){
    return await this.entityManager.getRepository(Benefit).createQueryBuilder().update().set(data)
    .where({id:id}).execute() 
}
async Find(){
    return this.entityManager.getRepository(Plan).createQueryBuilder().getMany()
}
async Delete (id:number,planId:number){
    return  this.entityManager.transaction(async(t)=>{
        await t.getRepository(Benefit).createQueryBuilder().relation(Plan,"benefit")
        .of(planId).set(null)
        await t.getRepository(Benefit).createQueryBuilder().delete().from(Benefit).where({id:id}).execute()
    })
}
}
export class ClientAdvantageSrv{
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
      ) {}
   async  Current(planId:number){
    return await this.entityManager.getRepository(Plan)
    .createQueryBuilder().relation(Plan,"benefit").of(planId)
    .loadMany()
}

   async subscribe(planId:number,UserId:CreateSubDto){
    return await this.entityManager.transaction(async(t)=>{
        console.log(planId,UserId)
        const NewPurchase=await t.getRepository(subscribtion).createQueryBuilder().insert()
        .into(subscribtion).values({expiresAt:addMonths(new Date(),UserId.month)}).execute()
        await t.getRepository(subscribtion).createQueryBuilder()
        .relation(subscribtion,'PalnId').of(planId).add(1) 
        await t.getRepository(User).createQueryBuilder()
        .relation(subscribtion,'subscribtion').of(UserId.userId).add(NewPurchase) 
    })
   
   }

}
