import { IsNotEmpty, IsString,IsNumber } from 'class-validator';
export class CreatePlanDto {
	@IsString()
	@IsNotEmpty()
	readonly name: string
	@IsString()
	@IsNotEmpty()
	readonly img: string
}
export class CreateBenefitDto {
	@IsNumber()
	@IsNotEmpty()
	readonly senderF: number
    @IsNumber()
	@IsNotEmpty()
	readonly depositF: number
    @IsNumber()
	@IsNotEmpty()
    readonly discount: number
    @IsNumber()
	@IsNotEmpty()
	readonly retreat: number
	@IsNumber()
	@IsNotEmpty()
	readonly plantId: number
}
//calcul nombre de month chaque subscriber
export class CreateSubDto {
	@IsNumber()
	@IsNotEmpty()
	readonly month: number
	@IsNumber()
	@IsNotEmpty()
	readonly userId: number
}