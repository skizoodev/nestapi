import {Module} from "@nestjs/common"
import { TypeOrmModule } from '@nestjs/typeorm';
import {JwtModule} from "@nestjs/jwt"
import {PassportModule} from "@nestjs/passport"
import {Plan,Benefit} from "../../@entity/plan/plan.entity"
import {AdminPlanCon} from "./controllers/plan.controller"
import {AdminBeneCon,ClientAdvantageCon} from "./controllers/advantage.controller"
import {AdminPlanServ} from "./services/plan.service"
import {AdminAdvantageSrv,ClientAdvantageSrv} from "./services/advantage.service"
@Module({
    imports:[ 
    PassportModule.register({defaultStrategy:"jwt"}),
    JwtModule.register({
    secret:"SOME DATA TYPES BREAKE OUT",
    signOptions:{expiresIn:"60min"}}),
    TypeOrmModule.forFeature([Plan,Benefit])],
    controllers:[AdminPlanCon,AdminBeneCon,ClientAdvantageCon],
    providers:[AdminPlanServ,AdminAdvantageSrv,ClientAdvantageSrv]
})
export class PlanModule{} 