import {
  Controller,
  Body,
  Post,
  Put,
  Delete,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,Request,
  ParseIntPipe,UseGuards
} from "@nestjs/common";
import {CreatePlanDto} from "../plan.dto";
import { AdminPlanServ} from "../services/plan.service";
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller("/amiro/valpha/plan")
export class AdminPlanCon {
  constructor(private readonly planServ: AdminPlanServ) {}

 @UseGuards(JwtAuthGuard)
@Roles(Role.User) 
  @Post("create")
  @HttpCode(201)
  async Create(@Body() arg:CreatePlanDto,@Request() req:any) {
    try {
      await this.planServ.Create(arg);
      return HttpStatus.CREATED;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_IMPLEMENTED);
    }
  }
  @UseGuards(JwtAuthGuard)
@Roles(Role.User)
  @Put("update")
  @HttpCode(202)
  async update(@Body() arg:CreatePlanDto) {
    try {
      await this.planServ.Update(arg);
      return HttpStatus.ACCEPTED;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Delete("delete")
  @HttpCode(204)
  async delete(@Param("id", ParseIntPipe) id: number) {
    try {
      await this.planServ.Delete(id);
      return HttpStatus.NO_CONTENT;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
}