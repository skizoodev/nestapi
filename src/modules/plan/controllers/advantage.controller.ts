import {
  Controller,
  Body,
  Post,
  Put,
  Delete,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  UseGuards,
  Get,
} from "@nestjs/common";
import { CreateBenefitDto,CreateSubDto} from "../plan.dto";
import {
  AdminAdvantageSrv,
  ClientAdvantageSrv,
} from "../services/advantage.service";
import { JwtAuthGuard } from "../../../guard/local-auth.guard";
import { Roles } from "../../../decorator/roles.decorator";
import { Role } from "../../../decorator/role.enum";
@Controller("amiro/valpha/advantage")
export class AdminBeneCon {
  constructor(private readonly planServ: AdminAdvantageSrv) {}
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Post("create")
  @HttpCode(201)
  async Create(
    @Body() arg: CreateBenefitDto  ) {
    try {
      await this.planServ.create(arg);
      return HttpStatus.CREATED;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_IMPLEMENTED);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Get("get/all")
  @HttpCode(201)
  async Find() {
    try {
    const data=  await this.planServ.Find();
      return HttpStatus.OK,data;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.NOT_IMPLEMENTED);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Put("update")
  @HttpCode(202)
  async update(@Body() arg:CreateBenefitDto,@Param("id", ParseIntPipe) id: number) {
    try {
      await this.planServ.Update(arg,id);
      return HttpStatus.ACCEPTED;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Delete("delete")
  @HttpCode(204)
  async delete(@Param("id/planId", ParseIntPipe) id: number,planId:number) {
    try {
      await this.planServ.Delete(id,planId);
      return HttpStatus.NO_CONTENT;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
}
@Controller("api/v1/plan")
export class ClientAdvantageCon{
  constructor(private readonly planServ:ClientAdvantageSrv) {}
  @Get("current/:planId")
  @HttpCode(201)
  async plan(@Param("planId",ParseIntPipe) planId:number) {
    try {
     const paln= await this.planServ.Current(planId);
     console.log(paln)
      return HttpStatus.FOUND,paln;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Post("subscribe/:planId")
  @HttpCode(204)
  async subscribe(@Param("planId",ParseIntPipe) planId:number,@Body()userId:CreateSubDto) {
    console.log(typeof(planId))
    try {
     const paln= await this.planServ.subscribe(planId,userId);
      return HttpStatus.ACCEPTED,paln;
    } catch (error) {
      throw new HttpException(`${error.message}`, HttpStatus.BAD_REQUEST);
    }
  }
  
}
