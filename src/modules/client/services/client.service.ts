import { Injectable } from '@nestjs/common';
import {InjectEntityManager } from '@nestjs/typeorm';
import {User} from "../../../@entity/clients/profile.entity"
import {Amount} from "../../../@entity/currency/amount.entity"
import {EntityManager} from 'typeorm';
import {Register,Updateo} from "../user.dto"
import { Auth } from '../../../@entity/clients/security.entity';
import {Email } from '../../../@entity/app/chat.entity';
@Injectable()
export class ClientService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
        ){}
async CreateUser(fata:Register){
  const mail=fata.phone+"@tunisio.tn"
 return this.entityManager.transaction(async(t)=>{
    const initialize=await t.getRepository(Amount)
    .createQueryBuilder().insert().values({}).execute()
  const client=  await t.getRepository(User).createQueryBuilder().insert().values({
      cin:fata.cin,
      fname:fata.fname,
      lname:fata.lname,
      avatar:fata.avatar,
      email:mail,
      birthday:new Date(fata.birthday),
      }).execute() 
      //add currecny to current client
      await t.getRepository(User).createQueryBuilder()
      .relation(User,"currency").of(client.identifiers[0].id)
      .set(initialize.identifiers[0].id)  
      //set user plan
      await t.getRepository(User).createQueryBuilder()
      .relation(User,"plan").of(client.identifiers[0].id).set(1)
      //add user to auth
      await t.getRepository(Auth).createQueryBuilder()
      .relation(Auth,"cliendId").of(1).set(client.identifiers[0].id)
      //update state for user  
     await t.getRepository(Auth).createQueryBuilder()
        .update().set({role:'Pending'}).where({id:1}).execute()
      //send 1st mail 
      await t.getRepository(Email).createQueryBuilder().insert().values({
        content:"your code is ",
        title:"3asba lik"
      }).execute()  
  })
}
async UpdateFoto(fata:Updateo){
     return await this.entityManager.getRepository(User).createQueryBuilder().update()
     .set({avatar:fata.avatar}).where({id:fata.authId})
      .execute()
}
async FindUser(fata:Updateo):Promise<any>{
return await this.entityManager.getRepository(User).createQueryBuilder().select()
  .where(fata).getMany()
}
async AddUser(userId:1,FriendId:2):Promise<any>{
  return await this.entityManager.getRepository(User).createQueryBuilder()
  .relation(User,"friends").of(userId).add(FriendId)
  }
  async blacklist(userId:1,FriendId:2):Promise<any>{
    return await this.entityManager.getRepository(User).createQueryBuilder()
    .relation(User,"friends").of(userId).add(FriendId)
    }
}