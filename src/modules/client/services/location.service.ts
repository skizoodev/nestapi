import { Injectable } from '@nestjs/common';
import {Tenant} from "../../../@entity/clients/addintional.entity"
import {User} from "../../../@entity/clients/profile.entity"
import {EntityManager} from "typeorm"
import {InjectEntityManager} from "@nestjs/typeorm"
import {Location} from "../user.dto"
@Injectable()
export class LocationService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
    ){}
async create(arg:Location,userId:1){
    return this.entityManager.transaction(async(t)=>{
        const address=await t.getRepository(Tenant).createQueryBuilder().insert().values(arg).execute()
        await t.createQueryBuilder().relation(User,'tenant_Id').of(userId).add(address.identifiers[0].id)
    })
}
async getAll(userId:1){
        return await this.entityManager.getRepository(User).createQueryBuilder()
        .relation(User,"tenant_Id").of(userId).loadMany()
    }
async delete(tenantId:1,userId:1){
    return this.entityManager.transaction(async(t)=>{
        await t.getRepository(User).createQueryBuilder().relation(User,"tenant_Id")
        .of(tenantId).set(null)
        await t.getRepository(Tenant).createQueryBuilder()
        .delete().from(Tenant).where({id:1}).execute()
    })
}
}
