import {IsNotEmpty,IsString} from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
export class Register{
	@IsString()
	@IsNotEmpty()
	readonly cin:string
    @IsString()
	@IsNotEmpty()
	readonly fname:string
    @IsString()
	@IsNotEmpty()
	readonly lname:string
    @IsString()
	@IsNotEmpty()
	readonly sexe:string
    @IsString()
	@IsNotEmpty()
	readonly avatar:string
    @IsString()
	@IsNotEmpty()
	readonly birthday:string
	@IsString()
	@IsNotEmpty()
	readonly phone:string
	@IsNotEmpty()
	readonly authId:number
}
export class Updateo extends PartialType(Register){}
export class Location {
	@IsString()
	@IsNotEmpty()
	readonly name:string
	@IsString()
	@IsNotEmpty()
	readonly img:string
	@IsString()
	@IsNotEmpty()
	readonly address:string
	@IsString()
	@IsNotEmpty()
	readonly zipcode:string
}
export class UpLoc extends PartialType(Location){
	id?:number
}