import { Controller,Body,Post,Delete,Put,Get,
    HttpCode,HttpException,HttpStatus
 } from '@nestjs/common';
import {LocationService} from "../services/location.service"
import {Location,UpLoc} from "../user.dto"
@Controller('api/v1/location')
export class LocationController {
constructor(private readonly addCon:LocationService){}
@Post("add")
@HttpCode(201)
async Add(@Body() arg:Location,userId:1){
try{
await this.addCon.create(arg,userId)
return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Get("find")
@HttpCode(201)
async Edite(userId:1){
    try{
    const adress=await this.addCon.getAll(userId)
    return  HttpStatus.ACCEPTED,adress
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Delete("byebye")
@HttpCode(201)
async ByeBye(tenantId: 1, userId: 1){
    try{
    await this.addCon.delete(tenantId,userId)
    return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}

