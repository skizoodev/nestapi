import {Body, Controller, Post,Get, Put
  ,HttpCode,HttpException,HttpStatus
} from "@nestjs/common"
import {ClientService} from "../services/client.service"
import {Register,Updateo} from "../user.dto"
@Controller("api/v1/user")
export class ClientCon{
constructor(private qutRepositry:ClientService){}
@Post("create")
@HttpCode(201)
async User(@Body() arg:Register){
  try{
    await this.qutRepositry.CreateUser(arg)
     return  HttpStatus.ACCEPTED
  } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
}
@Get("get")
@HttpCode(201)
async GetUser(@Body() arg:Updateo){
  try{
    const clients=await  this.qutRepositry.FindUser(arg)
    return  HttpStatus.ACCEPTED,clients
  } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
  }
@Put("update")
@HttpCode(201)
async UpdatUser(@Body() arg:Updateo){
  try{
      await  this.qutRepositry.UpdateFoto(arg)
      return  HttpStatus.ACCEPTED
    } catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
    } 
}