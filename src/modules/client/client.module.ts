import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm"
import {User } from "../../@entity/clients/profile.entity"
import {Tenant } from "../../@entity/clients/addintional.entity"
import { ClientService } from './services/client.service';
import { LocationService } from './services/location.service';
import {ClientCon} from "./controllers/client.controller";
import {LocationController} from "./controllers/location.controller";

@Module({
  imports:[TypeOrmModule.forFeature([User,Tenant])],
  controllers:[ClientCon,LocationController],
  providers: [ClientService,LocationService]
})
export class UserModule{}
