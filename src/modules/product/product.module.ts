import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Attribute,FaCategorie,SteFacture} from "../../@entity/ste-Services/ste-service.entity"
import {AttributeController} from "./controller/attribute.controller"
import {CategorieController} from "./controller/categorie.controller"
import {AdminProductController,ClientPorductCont} from "./controller/product.controller"
import {AttributeService} from "./services/attribute.service"
import {CategorieService} from "./services/categorie.service"
import {AdminProductService,ClinetProductService} from "./services/product.service"

@Module({
imports:[TypeOrmModule.forFeature([Attribute,FaCategorie,SteFacture])],
controllers:[AttributeController,CategorieController,AdminProductController,ClientPorductCont],
providers:[AttributeService,CategorieService,AdminProductService,ClinetProductService]
})
export class ProductModule {}
