import { Test, TestingModule } from '@nestjs/testing';
import { ClientPorductCont } from '../controller/product.controller';

describe('ProductController', () => {
  let controller: ClientPorductCont;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientPorductCont],
    }).compile();

    controller = module.get<ClientPorductCont>(ClientPorductCont);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
