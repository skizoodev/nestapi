import { Test, TestingModule } from '@nestjs/testing';
import { ClinetProductService } from '../services/product.service';

describe('ProductService', () => {
  let service: ClinetProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ClinetProductService],
    }).compile();

    service = module.get<ClinetProductService>(ClinetProductService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
