import { Body, Controller, Post,Get, Delete
    ,HttpCode,HttpException,HttpStatus
 } from '@nestjs/common';
import {AttributeService} from "../services/attribute.service"
import {facture} from "../product.dto"
@Controller('amiro/attribute')
export class AttributeController {
    constructor(
        private readonly attServ:AttributeService
    ){}
@Post("create")
@HttpCode(201)
async create(@Body() arg:facture){
try{
   await this.attServ.create(arg)
   return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Get("find")
@HttpCode(201)
async find(){
    try{
    const attribute=await this.attServ.get()
    return  HttpStatus.ACCEPTED,attribute
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
}
@Delete("remove")
@HttpCode(201)
async remove(){
    try{
    await this.attServ.delete()
    return  HttpStatus.OK
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
}
}
 