import { Controller ,Post,Body,Delete,Get
    ,HttpCode,HttpException,HttpStatus
} from '@nestjs/common';
import {AdminProductService,ClinetProductService} from "../services/product.service"
import {facture} from "../product.dto"
@Controller('amiro/product')
export class AdminProductController {
    constructor(
        private readonly proSer:AdminProductService
    ){}
@Post("create")
@HttpCode(201)
 async create(@Body() arg:facture){
    try{
    await this.proSer.create(arg)
    return HttpStatus.ACCEPTED
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Get("find")
@HttpCode(202)
 async get(){try{
    let product=await this.proSer.find()
    return HttpStatus.ACCEPTED,product
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Delete("remove")
@HttpCode(201)
async remove(){
    try{
     this.proSer.delete()
    return HttpStatus.ACCEPTED
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}
@Controller('api/v1/product')
export class ClientPorductCont {
    constructor(
        private readonly proSer:ClinetProductService
    ){}
@Get("find")
@HttpCode(202)
 async get(){
    try{
    let product=await this.proSer.find()
    return HttpStatus.ACCEPTED,product
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}

}