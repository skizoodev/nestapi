import { Controller,Post,Body,Put
    ,Get,HttpCode,HttpException,HttpStatus
} from '@nestjs/common';
import {CategorieService,ClientCategorieSer} from "../services/categorie.service"
import {categorie} from "../product.dto"
@Controller('amiro/categorie')
export class CategorieController {
    constructor(
        private readonly catServ:CategorieService
    ){}
@Post("create")
@HttpCode(201)
 async create(@Body() arg:categorie ){
    try {
        await this.catServ.create(arg)
        return HttpStatus.ACCEPTED
    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@Put("update")
@HttpCode(202)
async update(@Body() arg:categorie){
    try{
    await this.catServ.update(arg)
    return HttpStatus.ACCEPTED
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}
@Controller("api/v1/categorie")
export class ClientCategorieCon{
    constructor(
        private readonly catServ:ClientCategorieSer
    ){}
@Get("find")
async find(){
    try{
   const categories= await this.catServ.find()
    return HttpStatus.ACCEPTED,categories
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@Get("findfa")
async findFact(){
    try{
   let fa= await this.catServ.Getfacture()
    return HttpStatus.ACCEPTED,fa
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}}
}
