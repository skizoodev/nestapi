import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {Repository} from "typeorm"
import {Attribute} from "../../../@entity/ste-Services/ste-service.entity"
import {facture} from "../product.dto"
@Injectable()
export class AttributeService {
    constructor(
        @InjectRepository(Attribute)
        private readonly attribute:Repository<Attribute>
    ){}
async create(arg:facture){ 
       return await this.attribute.createQueryBuilder()
       .insert().values(arg).execute()
  }
async get(){ 
 return await this.attribute.createQueryBuilder().getMany()
}
async delete(){ 
    await this.attribute.createQueryBuilder().delete() .execute()
}
}
