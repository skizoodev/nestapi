import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {Repository} from "typeorm"
import {FaCategorie} from "../../../@entity/ste-Services/ste-service.entity"
import {categorie} from "../product.dto"
@Injectable()
export class CategorieService {
    constructor(
        @InjectRepository(FaCategorie)
        private readonly categorie:Repository<FaCategorie>
    ){}
async create(arg:categorie){
     await this.categorie.createQueryBuilder().insert().values({
            name:arg.name,
            img:arg.icon,
        }) .execute()
}
async update(arg:categorie){
      await  this.categorie.createQueryBuilder().update().set({
            name:arg.name,
            img:arg.icon
        }).where({name:arg.name}).execute()
        .then(()=>{
            return "updated"
        }).catch(()=> {
        return "error"
    })} 
}
export class ClientCategorieSer{
    constructor(
        @InjectRepository(FaCategorie)
        private readonly categorie:Repository<FaCategorie>
    ){}
async find(){
     return await this.categorie.createQueryBuilder().getMany()
}
async Getfacture(){
        return await this.categorie.createQueryBuilder()
        .relation(FaCategorie,"factures").of(1)
        .loadMany()
}
}
