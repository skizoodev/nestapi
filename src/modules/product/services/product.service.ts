import { Injectable } from '@nestjs/common';
import { InjectRepository,InjectEntityManager } from '@nestjs/typeorm';
import { DataSource, Repository,EntityManager } from "typeorm"
import { SteFacture, FaCategorie, Attribute } from "../../../@entity/ste-Services/ste-service.entity"
import { facture } from "../product.dto"

@Injectable()
export class AdminProductService {
    constructor(
        @InjectEntityManager()
        private readonly EntityManger: EntityManager,
    ){}
    async create(arg: facture) {
   return this.EntityManger.transaction(async(t)=>{
    let attribute = await t.getRepository(Attribute)
    .createQueryBuilder().insert().values({
        name: arg.attribute,
        value: arg.value
    }
    ).execute()
let product = await t.getRepository(SteFacture)
    .createQueryBuilder().insert().values({
        name: arg.name,
        img: arg.img,
        desc: arg.desc
    }
    ).execute()   
    await t.createQueryBuilder().relation(Attribute, 'facture')
    .of(attribute.identifiers[0].id).set(product.identifiers[0].id)
    await t.createQueryBuilder().relation(FaCategorie, 'factures')
    .of(arg.categories).add(product.identifiers[0].id)

})
    }
    async update(arg: facture) {
       return await this.EntityManger.createQueryBuilder().update().set({
            name: arg.name,
            img: arg.img
        }).where({ id: arg.name }).execute()
    }
    async find() {
            return await this.EntityManger.createQueryBuilder()
                .relation(SteFacture, "attributes").of(1)
                .loadOne()
    }
    async delete() {
        return this.EntityManger.transaction(async(r)=>{
            await r.createQueryBuilder().relation(Attribute, 'facture')
                .of(1).set(null)
            await r.createQueryBuilder().delete().
                from(SteFacture).where({ id: 1 }).execute()
            return "deleted"
        })
}
}
@Injectable()
export class ClinetProductService {
    constructor(
        @InjectRepository(SteFacture)
        private readonly fact: Repository<SteFacture>,
    ) { }
    async find() {
            return await this.fact.createQueryBuilder()
                .relation(SteFacture, "attributes").of(1)
                .loadOne()
    }

}
