import {IsString, IsNotEmpty, IsArray} from "class-validator"

export class facture{
    @IsString()
    @IsNotEmpty()
    name:string
    @IsString()
    @IsNotEmpty()
    desc:string
    @IsString()
    @IsNotEmpty()
    img:string
    @IsArray()
    @IsNotEmpty()
    categories:number[]
    @IsString()
    @IsNotEmpty()
   attribute:string
   @IsString()
   @IsNotEmpty()
   value:string
}
export class categorie{
    @IsString()
    @IsNotEmpty()
   name:string
   @IsString()
   @IsNotEmpty()
  icon:string
}
