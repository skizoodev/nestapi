import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {JwtModule} from "@nestjs/jwt"
import {PassportModule} from "@nestjs/passport"
import {Auth,EmailSec,Phone,Question,LoginLog} from "../../@entity/clients/security.entity"
import {AdminAuthController,ClientAuthController} from "./controllers/auth.controller"
import {AdminEmailSecurityCon,Email_Security_Con} from "./controllers/email.controller"
import {AdminPhoneSecurityCon,Phone_Security_Con} from "./controllers/phone.controller"
import {QSecurityCon} from "./controllers/question.controller"
import {AdminAuthService,ClientAuthService} from "./services/auth.service"
import {AdminEmailSerc,EmailSecServ} from "./services/email.service"
import {AdminPhoneServ,PhoneServ} from "./services/phone.service"
import {QSecurityServ} from "./services/question.service"
import {JwtStrategy} from "./Strategy/Jwt_Strategy"
import {LocalStrategy} from "./Strategy/Local_Strategy"
import {JwtAuthGuard} from "../../guard/local-auth.guard"
@Module({
  imports: [
    PassportModule.register({defaultStrategy:"jwt"}),
    JwtModule.register({
      secret:"SOME DATA TYPES BREAKE OUT",
      signOptions:{expiresIn:"60min"}}),
    TypeOrmModule.forFeature([Auth,EmailSec,Phone,Question,LoginLog])],
  controllers:[AdminAuthController,ClientAuthController
    ,Email_Security_Con,AdminEmailSecurityCon,
    AdminPhoneSecurityCon,Phone_Security_Con,
    QSecurityCon
  ],
  providers:[LocalStrategy,
    AdminAuthService,ClientAuthService,
    AdminEmailSerc,EmailSecServ,
    AdminPhoneServ,PhoneServ,QSecurityServ,JwtStrategy,JwtAuthGuard
  ],
  exports:[JwtStrategy,LocalStrategy,PassportModule,JwtAuthGuard]
})
export class AuthModule {}
