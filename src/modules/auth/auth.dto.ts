import { IsNotEmpty, IsString,MaxLength,MinLength,Matches,IsNumber} from 'class-validator';
import {  PartialType} from "@nestjs/mapped-types"
//dto for register
export class Register {
	@IsString()
	@IsNotEmpty()
	@MinLength(4,{message:"username must be more than 4 characteur"})
	@MaxLength(25,{message:"username must be less than 25 characteur"})
	@Matches(/^[a-zA-Z][a-zA-Z0-9_-]{2,19}$/,{message:"username not valid"})
	readonly username: string
	@IsString()
	@IsNotEmpty()
	@MinLength(4,{message:"password must be more than 4 characteur"})
	@MaxLength(25,{message:"must be less than 25 characteur"})
	@Matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,{message:"password format not valid"})
	readonly password: string
}
//dto to verfy the auth change 
export class Modify2col {
	@IsNotEmpty({message:"please write somthing her "})
	@IsString({message:"this filed accept only text "})
	@MinLength(4,{message:"password must be more than 4 characteur"})
	@MaxLength(25,{message:"must be less than 25 characteur"})
	@Matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,{message:"password format not valid"})
	readonly secret:string
	@IsNotEmpty({message:"please write somthing her "})
	@IsString({message:"this filed accept only text "})
	readonly renw:string
}
// dto for addidng an email 
export class CreateEmailDto extends PartialType(Modify2col){
	@IsNotEmpty()
	@IsString()
	@Matches(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
	,{message:"must be a valid email format"})
	readonly email:string
}
// dto for addidng an phone 
export class CreatePhoneDto extends PartialType(Modify2col){
	@IsNotEmpty()
	@IsString()
	@Matches(/\d{2}-\d{3}-\d{3}/
	,{message:"must be a valid phone format"})
	readonly phone:string
}
// dto for addidng an a qustion
export class dtoQution{
	@IsNotEmpty()
	@IsString()
	readonly question:string
	@IsString()
	@IsNotEmpty()
	readonly response:string
}
//dto for Admin auth
export class UpdateAuthDto{
	@IsNotEmpty()
	@IsString()
	readonly userId:string
	@IsString()
	@IsNotEmpty()
	readonly renw:string
}
//for admin to delete user Data
export class RemoveDtoData{
	@IsNotEmpty()
	@IsNumber()
	readonly dataId:number
	@IsNumber()
	@IsNotEmpty()
	readonly userId:number
}
//for use th check the qustion answer
export class CreateDtoAnswer{
	@IsNotEmpty()
	@IsString()
	readonly response:string
	@IsString()
	@IsNotEmpty()
	readonly result:string
}