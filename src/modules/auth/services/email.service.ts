import {Injectable,NotFoundException} from '@nestjs/common';
import { InjectEntityManager} from '@nestjs/typeorm';
import {EntityManager,EntityNotFoundError } from 'typeorm';
import {EmailSec,Auth} from "../../../@entity/clients/security.entity"
import {CreateEmailDto, RemoveDtoData} from "../auth.dto"
@Injectable()
export class EmailSecServ{
constructor(
    @InjectEntityManager()
        private readonly entityManager:EntityManager
    ){}
async CreateEmail(arg:CreateEmailDto,userId:any):Promise<any>{
        return  this.entityManager.transaction(async(t)=>{
            const added=  await t.getRepository(EmailSec).createQueryBuilder().
            insert().into(EmailSec).values({email:arg.email}).execute()
            await t.getRepository(EmailSec).createQueryBuilder()
              .relation(Auth,"esId").of(userId).add(added.raw.insertId)  
          })
}
async ConfirmEmail(arg:CreateEmailDto){
    return await this.entityManager.getRepository(EmailSec).createQueryBuilder().update()
    .set({isverfied:true})
    .where({email:arg.email}).execute()
}
async FindEmail(arg:CreateEmailDto){
    try {
        return await this.entityManager.getRepository(EmailSec).createQueryBuilder().
        where({email:arg.email,isverfied:true}).getOneOrFail()
    } catch (error) {
        if (error instanceof EntityNotFoundError) {
            // username not found
        throw new NotFoundException("this email not found ")
    } 
    }
 
} 
async GetUser( userId:any){
    return await this.entityManager.getRepository(Auth)
    .createQueryBuilder().relation(Auth,"esId").of(userId).loadMany()
   }
}
@Injectable()
export class AdminEmailSerc{
    constructor(
    @InjectEntityManager()
    private readonly entityManager:EntityManager
    ){}
async remove (arg:RemoveDtoData){
    return  this.entityManager.transaction(async(t)=>{
        await t.getRepository(EmailSec).createQueryBuilder().relation(EmailSec,"authId")
        .of(arg.userId).set(null)
        await t.getRepository(EmailSec).createQueryBuilder().delete().from(EmailSec).where({id:arg.dataId}).execute()
    })
}
}
