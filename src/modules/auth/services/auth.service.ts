import { Injectable,UnauthorizedException,NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository,EntityNotFoundError } from 'typeorm';
import { Auth,AuthState,LoginLog} from "../../../@entity/clients/security.entity"
import { UpdateAuthDto, Register, Modify2col } from "../auth.dto"
import { hashSync, compareSync,genSaltSync } from "bcrypt"
const salt=genSaltSync(10)
@Injectable()
export class ClientAuthService {
    constructor(
      @InjectRepository(Auth)
      private readonly auth:Repository<Auth>,
      @InjectRepository(LoginLog)
      private readonly agent:Repository<LoginLog>,
      ) {}
 
  async Sign(user: Register):Promise<string>{
    const encrypted:string= Math.floor(Math.random() * 9999).toString();
    const pwd=hashSync(user.password,salt),
          sct=hashSync(encrypted,salt);
      const created=await this.auth.createQueryBuilder()
      .insert().values({
        username: user.username.toLowerCase(),
        password:pwd,
        secret:sct
      }).execute()
        return created.identifiers[0].id
  }
  async validateUser(username:string,password:string,req:any) {
    try {
      const test = await this.auth
      .createQueryBuilder().select()
      .where({username:username})
      .getOneOrFail()
       let check: boolean = compareSync(password, test.password)
      if (check) {
  /*       await this.agent.createQueryBuilder().insert().values({
          browser:req.browser,
          engine:req.engine,
          device:req.device
        }).execute() */
        return test
      }
      else {
        //block for "Invalid username or password"
        throw new UnauthorizedException()
      }
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
            // username not found
        throw new NotFoundException()
    } 
    }     
  }
  async ValidatePassword(userId:any,secret:any){
    const test=await this.auth
    .createQueryBuilder()
    .where({ id:userId })
    .getOneOrFail()
    let check: boolean = compareSync(secret, test.password)
    if ( check){
      return "done"
    }
    else{
      return "wrong password"
    }
  }
  async UsernameChange(secret:Modify2col,req:any) {
          await this.auth.createQueryBuilder().update().
            set({ username: secret.renw }).where({ id: req.userId })
            .execute()
          return "done ..!" 
  }
  async QetQustion() {
    //get question after login
  return await this.auth
  .createQueryBuilder().relation(Auth,"qsId").of(1).loadOne()
  }
  async SecretChange(secret: Modify2col,req:any): Promise<any> { 
          let ncode = hashSync(secret.renw, salt)
           await this.auth.createQueryBuilder().update().
            set({ secret: ncode }).where({ id:req.userId })
            .execute()
          return "done"
  }
  async PasswordChange(secret: Modify2col,req:any) {
          let ncode = hashSync(secret.renw, salt)
          await this.auth.createQueryBuilder().update().
            set({ password: ncode }).where({ id: req.userId })
            .execute()
          return "done ...!"  
  }

  async SoftDelete(){
    return await this.auth.createQueryBuilder().softDelete()
     .where({id:1}).execute()
   }
}
@Injectable()
export class AdminAuthService {
    constructor(
       @InjectRepository(Auth)
        private authRepository: Repository<Auth>
      ) { }
    async SecretForce(secret: UpdateAuthDto): Promise<any> {
          let ncode = hashSync(secret.renw, 12)
          await this.authRepository.createQueryBuilder().update().
            set({ secret: ncode }).where({ id: secret.userId })
            .execute()
          return "done ...!"
      
      }
      async PasswordForce(secret: UpdateAuthDto) {    
          let ncode: string = hashSync(secret.renw, 12)
          await this.authRepository.createQueryBuilder().update().
            set({ password: ncode }).where({ id: secret.userId })
            .execute()
          return "done ...!"
      }
      async UpdateRole(secret: UpdateAuthDto) {
         if(secret.renw && Object.values(AuthState).includes( secret.renw as unknown as AuthState)){
                    await this.authRepository.createQueryBuilder().update().
                      set({role:secret.renw}).where({ id: secret.userId })
                      .execute()
                    return "done ...!"
           }
      }
      async Remove(secret: UpdateAuthDto){
          await this.authRepository.createQueryBuilder().delete().where({id:secret.userId})
         .execute()
          return "user removed"
      }
      async GetbyState(secret: UpdateAuthDto){
          const Users=  await this.authRepository.createQueryBuilder("role").where({state:secret.renw})
          .orderBy("id","DESC").getMany()
          return Users
    }
}