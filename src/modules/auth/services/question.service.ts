import {Injectable,NotFoundException} from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager,EntityNotFoundError} from 'typeorm';
import {Question,Auth} from "../../../@entity/clients/security.entity"
import {dtoQution,CreateDtoAnswer} from "../auth.dto"
import {hashSync,compareSync} from "bcrypt"
@Injectable()
export class QSecurityServ{
constructor( 
@InjectEntityManager()
private readonly entityManager:EntityManager
){}
async CreateQtion(arg:dtoQution,userId:any):Promise<any>{
let res=hashSync(arg.response,7)
return  this.entityManager.transaction(async(t)=>{
    const added=  await t.getRepository(Question).createQueryBuilder().
    insert().into(Question).values({question:arg.question,response:res}).execute()
    await t.getRepository(Auth).createQueryBuilder()
      .relation(Auth,"qsId").of(userId).set(added.raw.insertId)  
  })
}
async GetQtion(userId:any):Promise<any>{
    try {
        return await this.entityManager.getRepository(Auth)
    .createQueryBuilder().relation(Auth,"qsId").of(userId).loadOne()
    } catch (error) {
        if (error instanceof EntityNotFoundError) {
            // Qustion not found
        throw new NotFoundException("this qustion not found ")
    } 
    }
} 
async HandelQtion(arg:CreateDtoAnswer):Promise<boolean>{
    console.log(arg)
    return compareSync(arg.response,arg.result)
}
async UpadteQtion(arg:dtoQution,userId:any):Promise<any>{
    let res=hashSync(arg.response,7)
    return await this.entityManager.getRepository(Question)
    .createQueryBuilder().update(Question)
    .set({question:arg.question,response:res})
    .where({id:userId}).execute()
}
}
