import {Injectable ,NotFoundException} from '@nestjs/common';
import { InjectEntityManager,} from '@nestjs/typeorm';
import {EntityManager,EntityNotFoundError } from 'typeorm';
import {Phone,Auth} from "../../../@entity/clients/security.entity"
import {CreatePhoneDto,RemoveDtoData} from "../auth.dto"
@Injectable()
export class PhoneServ{
constructor(
    @InjectEntityManager()
        private readonly entityManager:EntityManager
    ){} 
async AddPhone(arg:CreatePhoneDto,userId:any):Promise<any>{
    return  this.entityManager.transaction(async(t)=>{
      const added=  await t.getRepository(Phone).createQueryBuilder().
      insert().into(Phone).values({phone:arg.phone}).execute()
      await t.getRepository(Phone).createQueryBuilder()
        .relation(Auth,"psId").of(userId).add(added.raw.insertId)  
    })
}
async ConfirmPhone(arg:CreatePhoneDto){
    return await this.entityManager.getRepository(Phone).createQueryBuilder().update()
    .set({isverfied:true})
    .where({phone:arg.phone}).execute()
}
async FindPhone(arg:CreatePhoneDto){
    try {
        return await this.entityManager.getRepository(Phone).createQueryBuilder().
        where({phone:arg.phone,isverfied:true}).getOneOrFail()
    } catch (error) {
        if (error instanceof EntityNotFoundError) {
            // phone not found
        throw new NotFoundException("this phone not found ")
    } 
    }

} 
async GetUser(userId:any){
    let user= await this.entityManager.getRepository(Auth)
    .createQueryBuilder().relation(Auth,"phones").of(userId).loadMany()
    return user
   } 
}
@Injectable()
export class AdminPhoneServ{
    constructor(
    @InjectEntityManager()
    private readonly entityManager:EntityManager
    ){}
async remove (arg:RemoveDtoData){
    return  this.entityManager.transaction(async(t)=>{
        await t.getRepository(Phone).createQueryBuilder().relation(Phone,"authId")
        .of(arg.userId).set(null)
        await t.getRepository(Phone).createQueryBuilder().delete().from(Phone).where({id:arg.dataId}).execute()
    })
}
}
