import {Body, Controller, Post,Get, Put,UseGuards,Request
  ,HttpCode,HttpException,HttpStatus
} from "@nestjs/common"
import {QSecurityServ} from "../services/question.service"
import {dtoQution,CreateDtoAnswer} from "../auth.dto"
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller("/api/v1/SecQuestion")
export class QSecurityCon{
constructor(private qutRepositry:QSecurityServ){}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Post("create")
@HttpCode(201)
async ReqQution(@Body() arg:dtoQution,@Request() req:any){
  try{
    const userId=req.userId
    await this.qutRepositry.CreateQtion(arg,userId)
    return  HttpStatus.ACCEPTED
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("get")
@HttpCode(201)
async GetQution(@Request() req:any){
  try{
    const userId=req.userId
  const qustion=await this.qutRepositry.GetQtion(userId)
    return HttpStatus.ACCEPTED,qustion
  } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
  }
  @UseGuards(JwtAuthGuard)
@Roles(Role.User)
  @HttpCode(201)
@Post("check")
async checkQution(@Body() arg:CreateDtoAnswer){
  try{
    const check=await this.qutRepositry.HandelQtion(arg)
    return  HttpStatus.ACCEPTED,check
  } catch (error) {
    console.log(error)
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
  }
  @UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Put("question")
@HttpCode(201)
async UpdateQution(@Body() arg:dtoQution,@Request() req:any){
  try{
    const userId=req.userId
      await  this.qutRepositry.UpadteQtion(arg,userId)
      return  HttpStatus.ACCEPTED
    } catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
}