import {Body, Controller, Post,Get,Delete,Patch,UseGuards,Request,
  HttpCode,HttpException,HttpStatus} from "@nestjs/common"
import {PhoneServ,AdminPhoneServ} from "../services/phone.service"
import {CreatePhoneDto, RemoveDtoData} from "../auth.dto"
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {PasswordGuard} from "../../../guard/custom.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller("api/v1/phone/security")
export class Phone_Security_Con{
constructor(private phone:PhoneServ){}
@UseGuards(JwtAuthGuard,PasswordGuard)
@Roles(Role.User)
@Post("create")
@HttpCode(201)
async AddPhone(@Body() arg:CreatePhoneDto,@Request() req:any){
  try {
    const userId=req.userId
     await this.phone.AddPhone(arg,userId)
     return HttpStatus.ACCEPTED
      } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
}
@Get("get")
@HttpCode(201)
async GetPhone(@Body() arg:CreatePhoneDto){
  try{
    const isverfied=  this.phone.FindPhone(arg) 
    return HttpStatus.ACCEPTED,isverfied
   } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("all/phones")
@HttpCode(201)
async GetUser(@Request() req:any){
  const userId=req.userId
  try{
    const isverfied=this.phone.GetUser(userId) 
    return HttpStatus.ACCEPTED,isverfied
   } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
  @UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Patch("update")
@HttpCode(201)
async UpdatePhone(@Body() arg:CreatePhoneDto){
  try{
    await this.phone.ConfirmPhone(arg)
    return HttpStatus.ACCEPTED
    } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
    }
}
@Controller("amiro/valpha/phone/security")
export class AdminPhoneSecurityCon{
  constructor(private phone:AdminPhoneServ){}
  @UseGuards(JwtAuthGuard)
  @Roles(Role.SuperAdmin)
  @Delete("/byebye")
  @HttpCode(204)
  async SayBye(@Body() arg:RemoveDtoData){
try {
  await this.phone.remove(arg)
  return HttpStatus.ACCEPTED 
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
}