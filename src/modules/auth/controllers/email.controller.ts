import {Body, Controller, Post,Get,Put,Delete,Request,UseGuards,
  HttpCode,HttpException,HttpStatus} from "@nestjs/common"
import {EmailSecServ,AdminEmailSerc} from "../services/email.service"
import {CreateEmailDto, RemoveDtoData} from "../auth.dto"
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {PasswordGuard} from "../../../guard/custom.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller("api/v1/email/security")
export class Email_Security_Con{
constructor(private email:EmailSecServ){}
@UseGuards(JwtAuthGuard,PasswordGuard)
@Roles(Role.User)
@Post("add")
@HttpCode(201)
async EmailCreation(@Body() arg:CreateEmailDto,@Request() req:any){
  try {
    const userId=req.userId
     await this.email.CreateEmail(arg,userId)
     return HttpStatus.ACCEPTED
      } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
  }
}
@Get("get")
@HttpCode(201)
async GetEmail(@Body() arg:CreateEmailDto){
  try{
    const isverfied=  this.email.FindEmail(arg) 
    return HttpStatus.ACCEPTED,isverfied
   } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("mails/all")
@HttpCode(201)
async GetEmails(@Request() req:any){
  try{
    const userId=req.userId
    const emails=  this.email.GetUser(userId) 
    return HttpStatus.ACCEPTED,emails
   } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
@Put("update")
@HttpCode(201)
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
async UpdateEmail(@Body() arg:CreateEmailDto){
  try{
    await this.email.ConfirmEmail(arg)
    return HttpStatus.ACCEPTED
    } 
    catch (error) {
      throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
    }
}
@Controller("amiro/valpha/email/security")
export class AdminEmailSecurityCon{
  constructor(private email:AdminEmailSerc){}
  @UseGuards(JwtAuthGuard)
  @Roles(Role.SuperAdmin)
  @Delete("/byebye")
  @HttpCode(204)
  async SayBye(@Body()arg:RemoveDtoData){
try {
  await this.email.remove(arg)
  return HttpStatus.ACCEPTED 
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
}