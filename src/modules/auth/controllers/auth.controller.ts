import { Controller,Request,
     Post ,Body,Patch,UseGuards,
    Delete,Get,HttpCode,HttpException,HttpStatus,ConflictException,
} from '@nestjs/common';
import {JwtService}from "@nestjs/jwt"
import {Register,UpdateAuthDto,Modify2col } from '../auth.dto';
import {ClientAuthService,AdminAuthService} from "../services/auth.service"
import {LocalAuthGuard,JwtAuthGuard} from "../../../guard/local-auth.guard"
import {PasswordGuard} from "../../../guard/custom.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller('api/v1/auth')
export class ClientAuthController {
   constructor (
    private readonly  authRpositry:ClientAuthService,
    private  jwtToken:JwtService
    ){}
@Post("/signup")
@HttpCode(201)
async register(@Body() arg:Register):Promise<any>{
  try {
        const signup=await this.authRpositry.Sign(arg)
         const refreshToken:string= this.jwtToken.sign({userId:
            signup,expiresIn:"1h"}) 
            return HttpStatus.CREATED, refreshToken
     } catch (error) {
        if(error.sqlState==="23000"){
            throw new ConflictException(`${error.message}`)
        }
    } 
}
@UseGuards(LocalAuthGuard)
@Post("signin")
@HttpCode(200)
async logIn( @Request() req:any ):Promise<any>{
  const user =req.user
  const refreshToken:string= this.jwtToken.sign({userId:
    user.id,expiresIn:"1h"})
    return HttpStatus.OK,{refreshToken:refreshToken,user:user} 
}
//get the security question 
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("Get")
@HttpCode(200)
async Get(){
    try {
      const signin= await this.authRpositry.QetQustion()
      return HttpStatus.OK,signin
    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@UseGuards(JwtAuthGuard,PasswordGuard)
@Roles(Role.User)
@Patch("/code")
@HttpCode(204)
async code(@Body() arg:Modify2col,@Request() req:any):Promise<any>{
try {
    await this.authRpositry.SecretChange(arg,req)
    return HttpStatus.NO_CONTENT
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard,PasswordGuard)
@Roles(Role.User)
@Patch("/password")
@HttpCode(204)
async password( @Body() arg:Modify2col,@Request() req:any):Promise<any>{
    try {
      await  this.authRpositry.PasswordChange(arg,req)
      return HttpStatus.NO_CONTENT

    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@UseGuards(JwtAuthGuard,PasswordGuard)
@Roles(Role.User)
@Patch("/surname")
async surname(@Body() arg:Modify2col,@Request() req:any):Promise<any>{
    try {
       const data= await this.authRpositry.UsernameChange(arg,req)
        return HttpStatus.NO_CONTENT,data
    } catch (error) {
       throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
}
@Controller('amiro/v1/auth')
export class AdminAuthController {
    constructor (
private readonly authAdmin:AdminAuthService
    ){}
    @Patch("/force/code")
    @HttpCode(204)
  async fcode(@Body() arg:UpdateAuthDto){
    try {
        await this.authAdmin.SecretForce(arg)
        return HttpStatus.ACCEPTED
    } catch (error) {
     throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
    }
@Patch("/force/password")
@HttpCode(204)
async fPassword(@Body() arg:UpdateAuthDto){
    try {
        await  this.authAdmin.PasswordForce(arg)
        return HttpStatus.ACCEPTED
    } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@Patch("/update/role")
@HttpCode(204)
async Role(@Body() arg:UpdateAuthDto){
    try {
        await this.authAdmin.UpdateRole(arg)
        return HttpStatus.ACCEPTED
    } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@Delete("byebye")
@HttpCode(204)
async remove(@Body() arg:UpdateAuthDto ){
    try {
        await this.authAdmin.Remove(arg)
        return HttpStatus.ACCEPTED
    } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
@Get("get")
@HttpCode(204)
async users(@Body() arg:UpdateAuthDto){
    try {
       const clients= await this.authAdmin.GetbyState(arg)
        return HttpStatus.ACCEPTED,clients
    } catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
    }
}
}

