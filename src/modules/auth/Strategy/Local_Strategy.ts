import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException,Request} from '@nestjs/common';
import { ClientAuthService } from '../services/auth.service';
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: ClientAuthService) {
    super();
  }
  async validate(username:string,secret:string,@Request() req:any): Promise<any> {
      const user = await this.authService.validateUser(username,secret,req);
      if (user) {
        return user;
      }
        throw new UnauthorizedException();  
  }
}