import { Injectable } from '@nestjs/common';
import {EntityManager, Repository} from 'typeorm';
import {InjectEntityManager,InjectRepository} from "@nestjs/typeorm"
import {Amount,Invoice,Recu} from "../../../@entity/currency/amount.entity"
import {deposit} from "../financial.dto"
@Injectable()
export class AmountService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
    ){}
    async deposit(arg:deposit){
        return this.entityManager.transaction(async (t) => {
                await t.decrement(Amount, { id: arg.userId }, "solde", arg.amount);
              const recu=  await  t.getRepository(Recu).createQueryBuilder().insert().values({
                    faAmount:arg.amount.toString(),
                    faDetail:"votre paiement a été crédité sur votre compte avec succès.",
                    faFrais:"0,500",
                    type:"virement"
                }).execute()
                await t.getRepository(Recu).createQueryBuilder().relation(Recu,"userId").of(recu.identifiers[0].id)
                .add(arg.userId)
        })
    }
    async retrat(arg:deposit){
        return this.entityManager.transaction(async (t) => {
                await t.getRepository(Amount).decrement({id:arg.userId}
                    ,"solde",arg.amount)
                    const recu=  await  t.getRepository(Recu).createQueryBuilder().insert().values({
                        faAmount:arg.amount.toString(),
                        faDetail:"Le paiement a été crédité sur votre compte avec succès",
                        faFrais:"0,500",
                        type:"retreat"
                    }).execute()
                    await t.getRepository(Recu).createQueryBuilder().relation(Recu,"userId").of(recu.identifiers[0].id)
                    .add(arg.userId)
        })
     
    }
    async send(arg:deposit){
        return this.entityManager.transaction(async (t) => {
                await t.decrement(Amount,{id:arg.userId},"solde",arg.amount)
                await t.increment(Amount,{id:arg.userId},"pt",arg.pt||10)
                await t.increment(Amount,{id:arg.benefitId},"solde",arg.amount)
                const recu=  await  t.getRepository(Recu).createQueryBuilder().insert().values({
                    faAmount:arg.amount.toString(),
                    faDetail:"Vous avez émis un mandat à l'attention de..."+arg.benefitId,
                    faFrais:"0,500",
                    type:"envoyer"
                }).execute()
                await t.getRepository(Recu).createQueryBuilder().relation(Recu,"userId").of(recu.identifiers[0].id)
                .add(arg.userId)
        })
    
    }
    async achatS(){
        return this.entityManager.transaction(async (t) => {
            try {
                await t.decrement(Amount,{id:1},"solde",8)
                await t.increment(Amount,{id:1},"pt",8)
                await t.getRepository(Invoice).createQueryBuilder().insert()
                .values({}).execute()
                return "done ...!!"
            } catch (error) {
                await t.getRepository(Invoice).createQueryBuilder().insert()
                .values({}).execute()
                return "failed ...!!"

            }
        })
    }
    async achatP(){
        return this.entityManager.transaction(async (t) => {
            try {
                await t.decrement(Amount,{id:1},"solde",8)
                await t.increment(Amount,{id:1},"pt",8)
                await t.getRepository(Invoice).createQueryBuilder()
                .insert().values({}).execute()
                return "done"
            } catch (error) {
                await t.getRepository(Invoice).createQueryBuilder()
                .insert().values({}).execute()
                return "failed"
            }
           
        })
   } 
}
export class AdminAmount{
    constructor(
        @InjectRepository(Amount)
        private readonly amountManger:Repository<Amount>
    ){}
    async getAll(){
      //  this.amountManger.createQueryBuilder().
    }
}