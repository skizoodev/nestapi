import { UseGuards,
    Controller,Body,Post,HttpCode,HttpException,HttpStatus
} from '@nestjs/common';
import {AmountService} from "../services/amount.service"
import {deposit} from "../financial.dto"
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
@Controller('api/v1/finacial')
export class AmountController {
    constructor(
        private readonly amountCon:AmountService
    ){} 
 
    @UseGuards(JwtAuthGuard)
    @Roles(Role.User)
    @Post("deposit")
    @HttpCode(201)   
     async Deposit(@Body() arg:deposit){
        try{
            this.amountCon.deposit(arg)
            return {status:HttpStatus.ACCEPTED,success:true,msg:"Votre montant a été ajouté à votre compte"}
    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.INTERNAL_SERVER_ERROR)
    }
    }
    @UseGuards(JwtAuthGuard)
    @Roles(Role.User)
    @Post("send")
    @HttpCode(201) 
    async send(@Body() arg:deposit){
        try{
          await this.amountCon.send(arg)
            return {status:HttpStatus.ACCEPTED,success:true,msg:"Vous avez envoyé un mandat à.."}
    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.INTERNAL_SERVER_ERROR)
    }  
  }
    @UseGuards(JwtAuthGuard)
    @Roles(Role.User)
    @Post("retreat")
    @HttpCode(201)  
    async retreat(@Body() arg:deposit){
        try{
          await this.amountCon.retrat(arg)
            return {status:HttpStatus.ACCEPTED,success:true,msg:"Les fonds ont été prélevés de votre compte."}
    } catch (error) {
        throw new HttpException(`${error.message}`,HttpStatus.INTERNAL_SERVER_ERROR)
    }  
  }
}
