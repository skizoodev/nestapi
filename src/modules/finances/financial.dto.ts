import {IsString,IsNotEmpty,IsNumber} from "class-validator"
export class deposit{
    @IsNumber()
	@IsNotEmpty()
	readonly amount:number
    @IsNumber()
	@IsNotEmpty()
	readonly userId:number
    @IsString()
    @IsNotEmpty()
    readonly benefitId:number
    @IsString()
    @IsNotEmpty()
    readonly type?:string
    @IsString()
    @IsNotEmpty()
    readonly status?:string
    @IsString()
    @IsNotEmpty()
    readonly detatil?:string
    @IsString()
    @IsNotEmpty()
    readonly url?:string
    @IsNumber()
    @IsNotEmpty()
    readonly pt?:number
}