import { Module } from '@nestjs/common';
import {AmountController} from "./controllers/amount.controller"
import {AmountService} from "./services/amount.service"
import {Amount,Invoice} from "../../@entity/currency/amount.entity"
import { TypeOrmModule } from '@nestjs/typeorm';
@Module({
    imports:[TypeOrmModule.forFeature([Amount,Invoice])],
    controllers:[AmountController],
    providers:[AmountService]

})
export class FinanceModule {}
