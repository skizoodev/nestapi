import { IsNotEmpty, IsString,IsNumber,IsDate } from 'class-validator';
import {  PartialType} from "@nestjs/mapped-types"
export class Create {
	@IsString()
	@IsNotEmpty()
	readonly name: string
	@IsString()
	@IsNotEmpty()
	readonly img: string
    @IsString()
	@IsNotEmpty()
	readonly for: string
	@IsNumber()
	@IsNotEmpty()
	readonly senderF: number
    @IsNumber()
	@IsNotEmpty()
	readonly depositF: number
    @IsNumber()
	@IsNotEmpty()
    readonly receiverMax: number
    @IsNumber()
	@IsNotEmpty()
	readonly bonus: number
    @IsNumber()
	@IsNotEmpty()
    readonly discount: number
    @IsNumber()
	@IsNotEmpty()
	readonly retreat: number
}
export class Update extends PartialType(Create){}

export class Delete{
	@IsNotEmpty()
	@IsNumber()
	id:number
}
export class Socail{
	@IsString()
	readonly title?: string
	@IsString()
	@IsNotEmpty()
	readonly content: string
	@IsString()
	@IsNotEmpty()
	readonly sender: string
	@IsString()
	@IsNotEmpty()
	readonly reciver: string
}
export class CrateSavingProdDTO{
	@IsString()
	@IsNotEmpty()
	readonly name: string
	@IsString()
	@IsNotEmpty()
	readonly img: string
	@IsString()
	@IsNotEmpty()
	readonly description: string
	@IsNumber()
	@IsNotEmpty()
	readonly amount: number
	@IsNumber()
	@IsNotEmpty()
	readonly minbudget:number
}
export class CrateSavingCatDTO extends PartialType(CrateSavingProdDTO){}
export class CreateCalanderDto{
	@IsNotEmpty()
	@IsString()
	readonly title: string
	@IsString()
	@IsNotEmpty()
	readonly detail: string
	@IsString()
	@IsNotEmpty()
	readonly filter:string
	@IsDate()
	@IsNotEmpty()
	readonly dated:Date
	@IsDate()
	@IsNotEmpty()
	readonly datef:Date
}
export class UpdateCalanderDto extends PartialType(CreateCalanderDto){}
