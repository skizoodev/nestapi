import {Module} from "@nestjs/common"
import { TypeOrmModule } from '@nestjs/typeorm';
import {JwtModule} from "@nestjs/jwt"
import {PassportModule} from "@nestjs/passport"

import {Email,Chat} from "../../@entity/app/chat.entity"
import {EmailController,ChatController} from "./controllers/comunity.controller"
import {ChatService,EmailService} from "./services/comunity.service"
@Module({
    imports:[ 
        PassportModule.register({defaultStrategy:"jwt"}),
    JwtModule.register({
    secret:"SOME DATA TYPES BREAKE OUT",
    signOptions:{expiresIn:"60min"}}),
   TypeOrmModule.forFeature([Email,Chat])],
    controllers:[EmailController,ChatController],
    providers:[EmailService,ChatService]
})
export class AppWebModule{} 