import {UseGuards,
    Controller,Body,Post,Get,
    Delete,HttpCode,HttpException,HttpStatus
} from "@nestjs/common"
import {Socail} from "../app.dto"
import {JwtAuthGuard} from "../../../guard/local-auth.guard"
import {Roles } from '../../../decorator/roles.decorator';
import {Role } from '../../../decorator/role.enum';
import {ChatService,EmailService} from "../services/comunity.service"
@Controller('/api/v1/emails')
export class EmailController {
    constructor(private readonly email:EmailService){}
    @UseGuards(JwtAuthGuard)
    @Roles(Role.User)
        @Post("create")
    @HttpCode(201)
  async send(@Body() arg:Socail ){
    try{
        await this.email.Send(arg)
    return HttpStatus.ACCEPTED
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
  @UseGuards(JwtAuthGuard)
  @Roles(Role.User)
  @Get("findOnerec")
  @HttpCode(201)
async GetOneRec( ){
  try{
     let mail= await this.email.GetOneRec()
  return HttpStatus.ACCEPTED,mail
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("findOnesen")
@HttpCode(201)
async GetOneSen( ){
try{
   let mail= await this.email.GetOneSend()
return HttpStatus.ACCEPTED,mail
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("FindR")
@HttpCode(201)
async GetAllR(){
try{
   const mails= await this.email.GetAllR()
return HttpStatus.ACCEPTED,mails
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("findS")
@HttpCode(201)
async GetAllS(){
try{
  const mails=  await this.email.GetAllS()
return HttpStatus.ACCEPTED,mails
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Delete("byer")
@HttpCode(201)
async DeleteR(){
try{
    await this.email.DeleteR()
return HttpStatus.ACCEPTED
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Delete("byeS")
@HttpCode(201)
async DeleteS(){
try{
    await this.email.DeleteS()
return HttpStatus.ACCEPTED
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}
@Controller('api/v1/messages')
export class ChatController {
    constructor(private readonly chat:ChatService){}
    @UseGuards(JwtAuthGuard)
@Roles(Role.User)
    @Post("create")
    @HttpCode(201)
  async send(@Body() arg:Socail ){
    try{
        await this.chat.Send(arg)
    return HttpStatus.ACCEPTED
} catch (error) {
    throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
  }
  @UseGuards(JwtAuthGuard)
@Roles(Role.User)
  @Get("findOne")
  @HttpCode(201)
async GetOne( ){
  try{
     let mail= await this.chat.GetOneRec()
  return HttpStatus.ACCEPTED,mail
} catch (error) {
  throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("FindR")
@HttpCode(201)
async GetAllR(){
try{
   const mails= await this.chat.GetAllR()
return HttpStatus.ACCEPTED,mails
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Get("findS")
@HttpCode(201)
async GetAllS(){
try{
  const mails=  await this.chat.GetAllS()
return HttpStatus.ACCEPTED,mails
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Delete("byer")
@HttpCode(201)
async DeleteR(){
try{
    await this.chat.DeleteR()
return HttpStatus.ACCEPTED
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
@UseGuards(JwtAuthGuard)
@Roles(Role.User)
@Delete("byeS")
@HttpCode(201)
async DeleteS(){
try{
    await this.chat.DeleteS()
return HttpStatus.ACCEPTED
} catch (error) {
throw new HttpException(`${error.message}`,HttpStatus.BAD_REQUEST)
}
}
}
