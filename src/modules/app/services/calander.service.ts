import { Injectable } from '@nestjs/common';
import {EntityManager} from 'typeorm';
import {InjectEntityManager } from '@nestjs/typeorm';
import {CreateCalanderDto,UpdateCalanderDto} from "../app.dto"
import {User} from "../../../@entity/clients/profile.entity"
import {Calander} from "../../../@entity/app/calander.entity"
@Injectable()
export class CalanderService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
        ){}
        async create(arg:CreateCalanderDto,UserId:number){
            return this.entityManager.transaction(async(t)=>{
                let result=await t.getRepository(Calander).createQueryBuilder().insert().values(arg).execute()
                await t.createQueryBuilder().relation(User,"events").of(UserId).add(result.identifiers[0].id)
            })
            }  
        async update(arg:UpdateCalanderDto,EventId:number){
                  return await this.entityManager.getRepository(Calander).createQueryBuilder().
                  update(Calander).set(arg).where({id:EventId}).execute()
                }    
        async byeBye(EventId:number,UserId:number){
            return this.entityManager.transaction(async(t)=>{
                await t.createQueryBuilder().relation(User,"events").of(UserId).set(null)
                await t.getRepository(Calander).createQueryBuilder().delete()
                .from(Calander) .where({ id: EventId})
                .execute()
            })
        }              
    }