import { Injectable } from '@nestjs/common';
import {EntityManager} from 'typeorm';
import {InjectEntityManager } from '@nestjs/typeorm';
import {SavingCat,SavingProduct} from "../../../@entity/app/saving.entity"
import {User} from "../../../@entity/clients/profile.entity"
import {CrateSavingCatDTO,CrateSavingProdDTO} from "../app.dto"
@Injectable()
export class SavingService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
        ){}
async createCat(arg:CrateSavingCatDTO){
        return await this.entityManager.getRepository(SavingCat).createQueryBuilder().insert().values({
            name:arg.name,
            img:arg.img
        }).execute()
    }
async createProduct(arg:CrateSavingProdDTO,SavCat:number){
      return  this.entityManager.transaction(async(t)=>{
       const product= await t.getRepository(SavingProduct).createQueryBuilder().insert().values(arg).execute()
        t.getRepository(SavingCat).createQueryBuilder().relation(SavingProduct,"categories")
        .of(product.identifiers[0].id).add(SavCat)
    })
}
async GetAllSaver(){
    return await this.entityManager.getRepository(SavingProduct).createQueryBuilder().relation(SavingProduct,"categories")
    .of(1).loadMany()
}
async Subscribe(userId:number,savingId:number){
    return await this.entityManager.getRepository(User).createQueryBuilder()
    .relation(User,"joinner")
    .of(userId).add(savingId)
}
async FindJoin(userId:number){
    return await this.entityManager.getRepository(User).createQueryBuilder()
    .relation(User,"joinner")
    .of(userId).loadMany()
}
}
