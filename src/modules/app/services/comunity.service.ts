import { Injectable } from '@nestjs/common';
import {EntityManager} from 'typeorm';
import {InjectEntityManager } from '@nestjs/typeorm';
import {Chat, Email} from "../../../@entity/app/chat.entity"
import {User} from "../../../@entity/clients/profile.entity"
import {Socail} from "../app.dto"
@Injectable()
export class EmailService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
        ){}
    async Send(arg:Socail){
    return this.entityManager.transaction(async(t)=>{
        let result=await t.getRepository(Email).createQueryBuilder().insert().values({
            title:arg.title,
            content:arg.content
        }).execute()
        await t.createQueryBuilder().relation(User,"sentMails").of(arg.sender).add(result.identifiers[0].id)
        await t.createQueryBuilder().relation(User,"recivedMails").of(arg.reciver).add(result.identifiers[0].id)
    })
    }
    async GetOneRec(){
        return await this.entityManager.getRepository(Email).createQueryBuilder().relation(Email,"recivedMails")
    .of(1).loadOne()
}
async GetOneSend(){
    return await this.entityManager.getRepository(Email).createQueryBuilder().relation(Email,"sentMails")
.of(1).loadOne()
}
async GetAllR(){
    return await this.entityManager.getRepository(Email).createQueryBuilder().relation(User,"recipient")
    .of(1).loadMany()
}
async GetAllS(){
    return await this.entityManager.getRepository(Email).createQueryBuilder().relation(User,"sentMails")
    .of(1).loadMany()
}
async DeleteS(){
     return await this.entityManager.getRepository(Email).createQueryBuilder().relation(User,"sentMails")
    .of(1).set(null)
}
async DeleteR(){
    return await this.entityManager.getRepository(Email).createQueryBuilder().relation(User,"recipient")
   .of(1).set(null)
}
}
@Injectable()

export class ChatService {
    constructor(
        @InjectEntityManager()
        private readonly entityManager:EntityManager
        ){}
    async Send(arg:Socail){
    return this.entityManager.transaction(async(t)=>{
        let result=await t.getRepository(Chat).createQueryBuilder().insert().values({
            message:arg.content
        }).execute()
        await t.createQueryBuilder().relation(User,"sentChat").of(arg.sender).add(result.identifiers[0].id)
        await t.createQueryBuilder().relation(User,"recivedMesseges").of(arg.reciver).add(result.identifiers[0].id)
    })
    }
    async GetOneRec(){
        return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(Chat,"recivedMesseges")
    .of(1).loadOne()
}
async GetOneSen(){
    return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(Chat,"recivedMesseges")
.of(1).loadOne()
}
async GetAllR(){
    return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(User,"recivedMesseges")
    .of(1).loadMany()
}
async GetAllS(){
    return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(User,"sentChat")
    .of(1).loadMany()
}
async DeleteS(){
     return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(User,"sentChat")
    .of(1).set(null)
}
async DeleteR(){
    return await this.entityManager.getRepository(Chat).createQueryBuilder().relation(User,"recivedMesseges")
   .of(1).set(null)
}
}