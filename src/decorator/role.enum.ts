export enum Role {
    User = 'user',
    Admin = 'admin',
    SuperAdmin = 'superadmin',
    Moderator = 'moderator',
    Auther="auther",
    Member="member",
    Pending="pending"
  }