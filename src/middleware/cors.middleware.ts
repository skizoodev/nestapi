import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as cors from 'cors';

@Injectable()
export class CorsMiddleware implements NestMiddleware {
  private readonly corsOptions = {
    origin: true, // allow all origins
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    allowedHeaders: 'Content-Type,Authorization',
    exposedHeaders: 'Content-Length,Authorization',
    credentials: true,
    optionsSuccessStatus: 204,
    preflightContinue: true,
    maxAge: 3600,
  };

  use(req: Request, res: Response, next: NextFunction) {
    return cors(this.corsOptions)(req, res, next);
  }
}

