import { Catch, ExceptionFilter, HttpException, ArgumentsHost } from '@nestjs/common';
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const status = exception.getStatus();
    const message = exception.message;
    var msg="empty msg";
    switch(status){
      case 201:msg="user created successful"
      break;
      case 409:msg="alrady used"
      break;
      case 400:msg="you are inserted invalid data"
      break;
      case 401:msg="Unauthorized"
      break;
      case 404:msg="page not found"
      break;
      default:msg=message
    } 
    response.status(status).json({
      statusCode: status,
      message: msg,
      timestamp: new Date().toISOString(),
    });
  }
}
