import { Injectable, NestMiddleware,ForbiddenException } from '@nestjs/common';

import * as UAParser from 'ua-parser-js';

@Injectable()
export class UAParserMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    const parser = new UAParser();
    const userAgent = req.headers['user-agent'];
    const result = parser.setUA(userAgent).getResult();
    req.device = result.device;
    req.os = result.os;
    req.browser = result.browser;
   // console.log(result)
    next();
  /*  const isBrowser = !!result.browser.name;
     if (isBrowser) {
        req.device = result.device;
        req.os = result.os;
        req.browser = result.browser;
       // console.log(result)
        next();
        // handle browser request
      } else {
        console.log(result)
        return result
        //throw new ForbiddenException(result)
        // handle non-browser request
      }
    */
  }
}