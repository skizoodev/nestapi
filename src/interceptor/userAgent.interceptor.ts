import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';
import * as uaParser from 'ua-parser-js';
@Injectable()
export class UserAgentInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const request = context.switchToHttp().getRequest<Request>();
        if (request.path === '/auth/login') { // or any other condition
          const userAgent = request.headers['user-agent'];
          const ua = uaParser(userAgent);
          // use TypeORM to save the user agent data to the database
        }
        return next.handle();
      }
}
