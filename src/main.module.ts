import { Module, NestModule, MiddlewareConsumer} from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm"
import {ConfigModule} from "@nestjs/config"
import  helmet from 'helmet';
//instal middleware
import {CorsMiddleware} from "./middleware/cors.middleware"
import {UAParserMiddleware} from "./middleware/userAgent.middleware"
import {RateLimitMiddleware} from "./middleware/limation.middleware"
//entity and moudle for authentication
import {AuthModule} from './modules/auth/auth.module'
import {Auth,LoginLog,Question,EmailSec,Phone} from "./@entity/clients/security.entity"
//entity and moudle for Plan 
import {PlanModule} from "./modules/plan/plan.module"
import {Benefit,Plan,subscribtion} from "./@entity/plan/plan.entity"
//entity for communication
import {Chat,Email} from "./@entity/app/chat.entity"
import {AppWebModule} from "./modules/app/app.module"

@Module({
  imports: [
    
  ConfigModule.forRoot({
    
/*     envFilePath: '.development.env',
    isGlobal: true */
  }),
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.DATABASE_HOST,
    port: 3306,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    autoLoadEntities:false,
     entities:[Auth,
      EmailSec,Phone,Question,LoginLog ,
      Benefit,Plan,subscribtion,Email,Chat
      /*
      ,User,Additional,Friend,
      Tenant,Amount,
      Saving,Invoice,Attribute,FaCategorie,
      SteFacture */
    ],
    synchronize: true,
  }),
  AuthModule,PlanModule,AppWebModule/*,UserModule,
  ProductModule,SavingModule,FinanceModule */
],providers:[RateLimitMiddleware]
})
// set up the middleware as app cors comprison limiton helmet
export class MainModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {    
    const helmetOptions = {
      contentSecurityPolicy: {
        directives: {
          defaultSrc: ["'self'"],
          styleSrc: ["'self'", 'stackpath.bootstrapcdn.com'],
        },
      },
      frameguard:false
    };
/*     const compressionOptions = {
      filter: (req:Request, res:Response) => {
        if (req.headers['x-no-compression']) {
          // don't compress responses with the "x-no-compression" header
          return false;
        }else{
        return compression.filter(req, res);}
      },
      level: 6,
      // compress only these content types
      types: ['text/html', 'text/css', 'application/javascript'],
    }; */
    consumer.apply(
      helmet(helmetOptions)
    ,CorsMiddleware,UAParserMiddleware
   // compression(compressionOptions),

    ).forRoutes('*');
  }}

